var db = require('../db');

module.exports = function(passport, Strategy){
	passport.use('local-login', new Strategy(
  function(username, password, cb) {
    db.users.findByUsername(username, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (user.password != password) { return cb(null, false); }
      return cb(null, user);
    });
  }));


passport.use('local-signup', new Strategy(
  function(username, password, done) {
    db.users.findByUsername(username, function(err, user) {
      if (err) return done(err);
      if (user) return done(null, false)
      return db.users.saveUser(username, password, done)
      });
}));

passport.serializeUser(function(user, cb) {
  cb(null, user._id);
});

passport.deserializeUser(function(id, cb) {
  db.users.findById(id, function (err, user) {
    if (err) { return cb(err); }
    cb(null, user);
  });
});
}