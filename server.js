var express = require('express');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var exphbs  = require('express-handlebars');
var cookieSession = require('cookie-session')
var app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));

app.set('view engine', 'handlebars');

app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: false }));
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2'],
  resave: true,
  saveUninitialized: true
}))
app.use(require('body-parser').json())
app.use(passport.initialize());
app.use(passport.session());

var mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/my_db')
var User = require('./db/mongoose').Users;
var Comment = require('./db/mongoose').Comments

User.find({username:'jack'}, function(err, result){
  if (err) console.log(err)
  if (! result.length) User({'username':'jack', 'password':'secret'}).save(function(err){
    console.log(err)
  })
})

Comment.find({}, function(err, result){
  if (err) console.log(err)
  if (!result.length){
    Comment({name:'Yoda', comment: 'Fear is the path to the dark side. \
      Fear leads to anger. Anger leads to hate. Hate leads to \
      suffering.'}).save(function(err){if (err) console.log(err)})
    Comment({name:'DarthVader', comment:'I find your lack of faith\
     disturbing'}).save(function(err){ if (err) console.log(err) })
  }
})

//passport config
require('./config/passport')(passport, Strategy)

//my routes
require('./routes/routes')(app, passport)

app.listen(9988)
module.exports = app