var app = require('../server')
var supertest = require('supertest')
var agent = supertest.agent(app)
var User = require('../db/mongoose').Users;

describe('Login page test', ()=> {
    it('test "/" route redirects to login', (done)=> {
        agent
        .get('/')
        .expect((res)=> {
            expect(res.header.location).toEqual('/login')
            expect(res.statusCode).toEqual(302)
        })
        .end(done)
    })

    it('login page should contain login signup forms and button for guests', (done)=>{
        agent
        .get('/login')
        .expect((res)=> {
            expect(res.text).toMatch(/\<form action="\/login/)
            expect(res.text).toMatch(/\<input name="username/)
            expect(res.text).toMatch(/\<input name="password/)
            expect(res.text).toMatch(/\<form action="\/signup/)
            expect(res.text).toMatch(/href(\w)?=(\w)?\'\/main\'/)
        })
        .end(done)
    })
})

describe('test authentication:', ()=> {
    it('test login with valid user', (done)=>{
        agent
        .post('/login')
        .type('form')
        .send({'username':'jack', 'password':'secret'})
        .expect((res)=> {
            expect(res.statusCode).toEqual(302)
            expect(res.header.location).toEqual('/main')
        })   
        .end(done)
    })

    it('test logout redirects to login', (done)=> {
        agent
        .get('/logout')
        .expect((res)=> {
            expect(res.statusCode).toEqual(302)
            expect(res.header.location).toEqual('/login')
        })
        .end(done)
    })

    it('test login with invalid user', (done)=> {
        agent
        .post('/login')
        .send({'username':'marcus', 'password':'kati'})
        .expect((res)=> {
            expect(res.statusCode).toEqual(302)
            expect(res.header.location).toEqual('/login')
        })
        .end(done)
    })

})

describe('test signup:', ()=>{
    
    afterEach((done)=> {
        agent
        .get('/logout')
        .end(done)
    })
    
    afterAll((done)=> {
        User.find({username: 'aVeryCustomUsername'}).remove(done)
    })

    it('should let any username and password signup if not exist', (done)=>{
 		agent
		.post('/signup')
		.type('form')
		.send({'username':'aVeryCustomUsername', 'password':'aVerySecretPassword'})
        .expect((res)=> {
            expect(res.statusCode).toEqual(302)
            expect(res.header.location).toEqual('/main')
        })
        .end(done)   
    })

    it('should not leave an existing username to signup', (done)=>{
        agent
        .post('/signup')
        .send({username: 'jack', password: 'secret'})
        .expect((res)=> {
            expect(res.statusCode).toEqual(302)
            expect(res.header.location).toEqual('/login')
        })
        .end(done)
    })

})