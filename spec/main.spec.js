var app = require('../server')
var supertest = require('supertest')
var agent = supertest.agent(app)
var Comment = require('../db/mongoose').Comments

describe('check main page without login', ()=> {
    
    it('should respond with html', (done)=>{
        agent
        .get('/main')
        .expect('Content-type', /html/)
        .expect(200)
        .end(done)
    })

    it('should not contain form for quotes', (done)=> {
        agent
        .get('/main')
        .expect(200)
        .expect((res)=>{
            expect(res.text).not.toMatch(/\<form/)
        })
        .end(done)
    })

    describe('try to post a comment when not logged in', ()=> {
        beforeEach((done)=>{
            agent
            .post('/main')
            .type('form')
            .send({'name':'Just a guest', 'comment': 'A stupidstupid one'})
            .end(done)
        })

        afterEach((done)=> {
            Comment.find({name:'Just a guest'}).remove(done)
        })

        it('should not contain the comment send from guest', (done)=> {
            agent
            .get('/main')
            .expect((res)=> {
                expect(res.text).not.toMatch(/A stupidstupid /)
            })
            .end(done)
        })
    })
})

describe('test main page as logged in user', ()=> {
    beforeAll((done)=>{
        agent
        .post('/login')
        .type('form')
        .send({'username':'jack', 'password':'secret'})
        .end(done)
    })

    it('check if form for new quotes is there', (done)=>{
        agent
        .get('/main')
        .expect((res)=> {
            expect(res.text).toMatch(/\<form/)
        })
        .end(done)
    })

    describe('logged in user should be able to post', ()=> {
        beforeEach((done)=> {
            agent
            .post('/main')
            .send({'name':'Dark Lord', 'comment':'I am a naughty boy' })
            .end(done)
        })
        
        afterEach((done)=> {
            Comment.find({comment: 'I am a naughty boy'}).remove(done)
        })

        it('comment should be in my html', (done)=> {
            agent
            .get('/main')
            .expect((res)=> {
                expect(res.text).toMatch(/I am a naughty /)
            })
            .end(done)
        })
    })
    
})