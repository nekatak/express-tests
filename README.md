Elium-w4-project
-------------------------------------------
This assignment expands the app of the zell tutorial https://zellwk.com/blog/crud-express-mongodb using expressjs, 
mongoosejs, and passportjs.


Required packages
-------------------------------------------
Run 
```bash 
$ npm install
```
inside your project to install dependencies. 


Setting up the database
-------------------------------------------
You will use mongodb as a database. You can work with  an external service called 
https://mlab.com/. It's MongoDB as a service. You will use a regular MongoDB client to 
connect the database you will connect to the MLab service.

One of the first things you need to do is to create an account in MLab and set up 
you own database and users there.

If you want to run mongodb locally you will need a host like mongodb://localhost:27017/your_db_name.
Do not forget to install mongo server on your machine.

Either way, you need to connect mongoose in server.js file.

After that, you need to create two mongoose schemas one called Users and another called Comments and also you 
you need to export them. 
For a tutorial check this https://scotch.io/tutorials/using-mongoosejs-in-node-js-and-mongodb-applications.


Passport
----------------------------------------------
You will have to configure passport according to this example: https://github.com/passport/express-4.x-local-example. 
The configuration files are inside the config folder. Also complete the methods needed inside the db/users.js file.

Main App
----------------------------------------------
The main app will contain 3 main (GET) routes. '/login' which will include login and sign up forms. '/main' which will 
contain the quotes if the user is a guest and if he is a logged in user will also contain a form to post quotes.

Start the app
----------------------------------------------
To run your app
```bash
$ npm end
```
If you are making changes run nodemon
```bash
$ npm start
```
and for the tests
```bash
$ npm test
```


Extra points
----------------------------------------------
Try to make your have test-cov above 85%
To make this project even more challenging you can implement a button that erases current users last quote-comment 
and write the tests for it so you have again test-cov above 85%.

Have fun!!!