var Comment = require('../db/mongoose').Comments


module.exports = function(app, passport){
	// Define routes.
app.get('/', function(req, res){
	res.redirect('/login')
})

app.get('/main',
  function(req, resp) {
    Comment.find({}, function(err, res){
      if (err) console.log(err)
      else {
        if (req.user) resp.render('userhome', { 'comments':res });
        else resp.render('home', {'comments':res });
      }
    })  
  });

app.post('/main', function(req, resp){
  if ("user" in req.session.passport){
    Comment({name:req.body.name, comment:req.body.comment}).save(function(err){
      console.log(err)
      resp.redirect('/main')
    })
  }
  else resp.status(403).send('Restricted Access')  
})

app.get('/login',
  function(req, res){
    res.render('login');
  });
  
app.post('/login', 
  passport.authenticate('local-login', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/main');
  });

app.post('/signup', 
  passport.authenticate('local-signup', {failureRedirect: '/login'}),
  function(req, res){
    res.redirect('/main')
})
  
app.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/login');
  });

}