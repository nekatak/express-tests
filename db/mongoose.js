//mongoose staff ------------>
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var userSchema = new Schema({
  name: String,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true }
});

var Users = mongoose.model('Users', userSchema)
exports.Users = Users

var commentSchema = new Schema({
  name: { type: String, required: true },
  comment: { type: String, required: true }
})

var Comments = mongoose.model('Comments', commentSchema)

exports.Comments = Comments