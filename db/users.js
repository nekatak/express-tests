var mongoose = require('mongoose')

var User = require('../db/mongoose').Users;
var Comment = require('../db/mongoose').Comments

exports.findById = function(id, cb) {
  process.nextTick(function() {
    User.findById(id, function(err, res){
      if (err) console.log(err)
      if (res) cb(null, res['_id'])
      else cb(new Error('User ' + id + ' does not exist'))
    })
  });
}

exports.findByUsername = function(username, cb) {
  process.nextTick(function() {
    User.find({username:username}, function(err, result){
      if (err) console.log(err)
     for (var i = 0; i < result.length; i++){
        var record = result[i]
        if (record.username === username) {
          return cb(null, record)
        }
      }
      return cb(null, null);      
    })
  });
}  

exports.saveUser = function(username, password,done){
  User({'username':username, 'password':password}).save(function(err, res){
    if (err) throw err 
    done(null, res) 
  })
}

